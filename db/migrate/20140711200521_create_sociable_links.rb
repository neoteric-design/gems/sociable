class CreateSociableLinks < ActiveRecord::Migration[4.2]
  def change
    create_table :sociable_links do |t|
      t.string  :address
      t.integer :social_linkable_id
      t.string  :social_linkable_type
      t.integer :position
      t.string  :kind

      t.timestamps null: false
    end
  end
end
