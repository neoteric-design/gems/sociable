require "rails_helper"

module Sociable
  describe Handlers do
    describe "::parse" do
      it 'detects the first configured handler that matches the given address' do
        expect(Handlers.parse('joe@joe.com')).to be_a(Handlers::EmailAddress)
        expect(Handlers.parse('facebook.com/joe')).to be_a(Handlers::Facebook)
        expect(Handlers.parse('nonsense.com')).to be_a(Handlers::Generic)
      end
    end

    describe "::find" do
      before :each do
        Sociable.configuration.handlers = [:twitter, :instagram]
      end

      context 'when given string matches a configured handler' do
        it 'returns matching class' do
          expect(Handlers.find("Instagram")).to eq(Handlers::Instagram)
        end
      end

      context 'when given string does not match a configured handler' do
        it 'raises NameError' do
          expect { Handlers.find("Facebook") }.to raise_error(NameError)
        end
      end
    end

    describe "::include?" do
      before :each do
        Sociable.configuration.handlers = [:twitter, :instagram]
      end
      context 'when given a symbol' do
        context 'and is included' do
          it { expect(Handlers.include?(:instagram)).to be_truthy }
        end

        context 'and is not included' do
          it { expect(Handlers.include?(:facebook)).to be_falsey }
        end
      end

      context 'when given a string' do
        context 'and is included' do
          it { expect(Handlers.include?("Instagram")).to be_truthy }
        end

        context 'and is not included' do
          it { expect(Handlers.include?("Facebook")).to be_falsey }
        end
      end

      context 'when given a class' do
        context 'and is included' do
          it { expect(Handlers.include?(Handlers::Instagram)).to be_truthy }
        end

        context 'and is not included' do
          it { expect(Handlers.include?(Handlers::Facebook)).to be_falsey }
        end
      end

      context 'when given something else' do
        it 'raises ArgumentError' do
          expect { Handlers.include?({ class: 'Instagram'}) }.to raise_error(ArgumentError)
        end
      end
    end
  end
end