require 'rails_helper'

RSpec.describe "ActiveRecord Integration" do
  it "makes sociable avaiable to any model" do
    expect(ActiveRecord::Base).to respond_to(:sociable)
  end

  it "estabilishes the social_links association" do
    expect(Parent.new).to respond_to(:social_links)
  end

  describe "#sociable_primary" do
    let(:twitter_url) { 'https://twitter.com/SwiftOnSecurity' }
    let(:parent) { Parent.create(social_links: [Sociable::Link.new(address: twitter_url)]) }

    context 'when primary present' do
      it 'returns address of primary link' do
        expect(parent.sociable_primary(:twitter)).to eq(twitter_url)
      end
    end

    context 'when primary nil' do
      it 'returns nil' do
        expect(parent.sociable_primary(:email_address)).to be_nil
      end
    end
  end
end

