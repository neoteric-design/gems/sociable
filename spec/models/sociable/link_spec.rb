require 'rails_helper'

module Sociable
  RSpec.describe Link, :type => :model do
    it { is_expected.to respond_to :address }
    let(:link) { Link.new(address: address) }

    describe "scopes" do
      let!(:emails) do
        [
          Link.create!(address: 'joe@joe.com'),
          Link.create!(address: 'johm@joe.com')
        ]
      end

      let!(:webs) do
        [
          Link.create!(address: 'www.joe.com')
        ]
      end

      describe "::by" do
        context 'when given string' do
          it { expect(Link.by("EmailAddress")).to match_array(emails) }
        end

        context 'when given symbol' do
          it { expect(Link.by(:email_address)).to match_array(emails) }
        end
      end

      describe "::primary" do
        context 'when any' do
          it 'returns of first by kind' do
            expect(Link.primary(:email_address)).to eq(emails.first)
          end
        end

        context 'when none' do
          it 'returns nil' do
            expect(Link.primary(:facebook)).to be_nil
          end
        end
      end
    end


    context 'malformed kind' do
      let(:link) { Link.new(address: 'http://tumblr.com', kind: 'tumbr') }

      it 'does not blow up' do
        expect { link.handler }.to_not raise_error
        expect(link.handler).to be_a(Handlers::Generic)
      end

      it 'can be forced reparse the address' do
        expect(link.kind(reload: true)).to eq('Tumblr')
      end
    end

    context 'malformed address' do
      it "fails validation" do
        expect { Link.create!(address: "herky jerky dot com") }.to(
          raise_error(::ActiveRecord::RecordInvalid)
        )
      end
    end

    context "phone numbers" do
      let(:valid_phone1) { Link.new(address: "512-123-1234") }
      let(:valid_phone2) { Link.new(address: "(512) 123-1234") }
      let(:valid_phone3) { Link.new(address: "+44 512 13 1234") }
      let(:valid_phone4) { Link.new(address: "5121231234 ext. 123") }
      let(:web)          { Link.new(address: "sassafras.com") }

      it "detects" do
        [valid_phone1, valid_phone2, valid_phone3, valid_phone4].each do |valid|
          expect(valid.kind).to eq('PhoneNumber')
        end
        expect(web.kind).to eq('Generic')
      end
    end

    context "EmailAddress" do
      let(:address) { "joey@shabadoo.com" }
      specify { expect(link.handler).to be_a(Handlers::EmailAddress) }

      let(:address) { "madeline@cowie.me" }
      specify { expect(link.handler).to be_a(Handlers::EmailAddress) }
    end

    context "Facebook" do
      let(:address) { "www.facebook.com/jerry" }

      it "sets the handler/kind" do
        expect(link.handler).to be_a(Handlers::Facebook)
        expect(link.kind).to eq('Facebook')
      end

      it "has an icon name based on handler" do
        expect(link.icon_name).to eq(Sociable::Handlers::Facebook.new.icon_name)
      end
    end

    context "Youtube" do
      let(:address) { 'youtube.com/jerry' }
      it "detects kind" do
        expect(link.handler).to be_a(Handlers::Youtube)
      end

      let(:address) { 'youtu.be/profile/jerry' }
      it "detects kind" do
        expect(link.handler).to be_a(Handlers::Youtube)
      end
    end

    %w(Tumblr Twitter LinkedIn Vimeo Instagram).each do |site|
      context site do
        let(:address) { "#{site}.com/jerry" }
        it "detects kind" do
          expect(link.handler).to be_a("Sociable::Handlers::#{site}".constantize)
        end
      end
    end
  end
end
