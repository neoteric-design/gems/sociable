$:.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'sociable/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'sociable'
  s.version     = Sociable::VERSION
  s.authors     = ['Madeline Cowie']
  s.email       = ['madeline@cowie.me']
  s.homepage    = 'http://www.neotericdesign.com'
  s.summary     = 'Easy attachment of many social accounts/links to a model'
  s.description = "Yep that's what it do"

  s.files = Dir['{app,config,db,lib}/**/*', 'Rakefile', 'README.rdoc']
  s.test_files = Dir['spec/**/*']

  s.add_dependency 'font-awesome-rails', '~> 4.0'
  s.add_dependency 'rails', '>= 4.2.1'

  s.add_development_dependency 'rspec-rails'
  s.add_development_dependency 'sqlite3'
end
