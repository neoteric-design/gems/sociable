class SociableInput < Formtastic::Inputs::StringInput
  def to_html
    object.social_links.build
    input_wrapping do
      label_html <<
      widget
    end
  end

  private

  def widget
    template.content_tag :ol, class: 'sociable' do
      builder.semantic_fields_for(:social_links) do |row_builder|
        template.content_tag :li, class: 'sociable-link' do
          icon(row_builder.object) <<
          row_builder.label(:address, sublabel, sublabel_options) <<
          row_builder.text_field(:address, input_options) <<
          meta_fields(row_builder) <<
          controls(row_builder) <<
          errors_for(row_builder)
        end
      end <<
      add_link
    end
  end

  def controls(row_builder)
    template.content_tag(:div, class: 'row-controls') do
      template.link_to(template.fa_icon('trash-o'), '#', class: 'remove')
    end
  end

  def meta_fields(row_builder)
    row_builder.hidden_field(:position, class: 'position') <<
    row_builder.hidden_field(:_destroy, class: 'destroy')
  end

  def icon(social_link)
    template.sociable_icon(social_link, class: 'kind')
  end

  def errors_for(row_builder)
    row_builder.semantic_errors(:address)
  end

  def add_link
    template.link_to(template.fa_icon('plus-circle'), '#', class: 'add-sociable button')
  end

  def input_options
    { class: 'address' }.merge(options.fetch(:input_html, {}))
  end

  def sublabel
    options.fetch(:sublabel, 'URL, Email, or Phone')
  end

  def sublabel_options
    options.fetch(:sublabel_html, {})
  end
end

