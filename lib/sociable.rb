require 'sociable/active_record'
require 'sociable/engine'
require 'sociable/configuration'
require 'sociable/sortable'

require 'sociable/handlers'
require 'sociable/handlers/generic'
require 'sociable/handlers/email_address'
require 'sociable/handlers/phone_number'
require 'sociable/handlers/social_media'

autoload :SociableInput, 'inputs/sociable_input'

module Sociable
  def self.params
    { social_links_attributes: %i[address position _destroy id] }
  end
end
