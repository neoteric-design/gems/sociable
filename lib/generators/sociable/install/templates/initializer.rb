Sociable.configure do |config|
  # Add/remove handlers, custom handlers live under Sociable::Handlers
  # and should respond to self.matches?(address_string) and default_icon_name
  # config.handlers = [:phone_number, :email_address, :facebook, :twitter,
  #                     :tumblr, :linked_in, :youtube, :vimeo, :instagram]

  # Override Font Awesome icons
  # config.icons = { facebook: 'some-other-icon', tumblr: 'tumblr-alt' }
end
