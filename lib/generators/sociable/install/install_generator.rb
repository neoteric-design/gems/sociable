module Sociable
  class InstallGenerator < Rails::Generators::Base
    source_root File.expand_path("../templates", __FILE__)

    def copy_templates
      copy_file "initializer.rb", "config/initializers/sociable.rb"
    end

    def install_migrations
      rake "sociable:install:migrations"
    end

    def generate_admin
      generate 'sociable:active_admin' if defined?(ActiveAdmin)
    end
  end
end