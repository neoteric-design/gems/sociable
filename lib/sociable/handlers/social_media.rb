module Sociable::Handlers
  class Facebook < Generic
    def self.matches?(str)
      str =~ /facebook/i
    end

    def default_icon_name
      'facebook-square'
    end
  end

  class Twitter < Generic
    def self.matches?(str)
      str =~ /twitter/i
    end

    def default_icon_name
      'twitter-square'
    end
  end

  class Tumblr < Generic
    def self.matches?(str)
      str =~ /tumblr/i
    end

    def default_icon_name
      'tumblr-square'
    end
  end

  class LinkedIn < Generic
    def self.matches?(str)
      str =~ /linkedin/i
    end

    def default_icon_name
      'linkedin-square'
    end
  end

  class Youtube < Generic
    def self.matches?(str)
      str =~ /youtu\.be/i || str =~ /youtube\./i
    end

    def default_icon_name
      'youtube-square'
    end
  end

  class Vimeo < Generic
    def self.matches?(str)
      str =~ /vimeo/i
    end

    def default_icon_name
      'vimeo-square'
    end
  end

  class Instagram < Generic
    def self.matches?(str)
      str =~ /instagram/i
    end

    def default_icon_name
      'instagram'
    end
  end

  class Skype < Generic
    def self.matches?(str)
      str =~ /skype/i
    end

    def default_icon_name
      'skype'
    end
  end
end
