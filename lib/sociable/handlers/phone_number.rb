module Sociable::Handlers
  class PhoneNumber < Generic
    def self.matches?(str)
      # Purposefully loose matcher, considering all the int'l formats and that
      # we aren't trying to validate so much as separate from emails and urls.
      # Allows digits, the letters e,x,t and punctuation +,-,(,),#

      str =~ /^[0-9+\(\)#\s\/\.ext-]+$/i
    end

    def default_icon_name
      'phone'
    end

    def is_url?
      false
    end
  end
end
