module Sociable::Handlers
  class Generic
    def self.matches?(str)
      true
    end

    def kind
      self.class.name.demodulize
    end

    def icon_name
      Sociable.configuration.icons.fetch(kind.to_sym, default_icon_name)
    end

    def default_icon_name
      'link'
    end

    def is_url?
      true
    end
  end
end
