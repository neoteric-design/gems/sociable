module Sociable::Handlers
  class EmailAddress < Generic
    def self.matches?(str)
      str =~ /.*@.*\..*/i
    end

    def default_icon_name
      'envelope'
    end

    def is_url?
      false
    end
  end
end
