module Sociable
  module ActiveRecord
    module ClassMethods
      def sociable
        include InstanceMethods

        has_many :social_links,
                 :class_name => '::Sociable::Link',
                 :as         => :social_linkable,
                 :dependent  => :destroy,
                 :validate   => true

        accepts_nested_attributes_for :social_links,
          :reject_if => lambda { |a| a[:address].blank? },
          :allow_destroy => true
      end
    end

    module InstanceMethods
      def sociable_primary(kind)
        social_links.primary(kind)&.address
      end
    end
  end
end
