module Sortable
  def self.included(base)
    base.send(:default_scope, -> { base.order(:position) })
    base.send(:before_create, :set_position)
  end

  private
  def set_position
    self[:position] ||= next_position
  end

  def next_position
    highest = self.class.maximum(:position) || -1
    highest + 1
  end
end
