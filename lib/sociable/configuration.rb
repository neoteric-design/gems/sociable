module Sociable
  class Configuration
    attr_accessor :icons, :handlers

    def initialize
      self.handlers ||= [:phone_number, :email_address, :facebook, :twitter,
                      :tumblr, :linked_in, :youtube, :vimeo, :instagram]
      @icons ||= {}
    end


    def handlers=(handlers_list)
      @handlers = Array[handlers_list].flatten.push(:generic).uniq
    end
  end

  class << self
    attr_writer :configuration
    def configuration
      @configuration ||= Configuration.new
    end

    def reset
      @configuration = Configuration.new
    end

    def configure
      yield(configuration)
    end
  end
end