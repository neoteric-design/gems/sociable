module Sociable
  module Handlers
    def self.configured_handlers
      Sociable.configuration.handlers.map { |h| "Sociable::Handlers::#{h.to_s.classify}".constantize }
    end

    def self.find(str)
      raise NameError unless include?(str)
      "Sociable::Handlers::#{str.classify}".constantize
    end

    def self.parse(address)
      match = configured_handlers.detect { |handler| handler.matches?(address) }
      match.new
    end

    def self.include?(kind)
      case kind.class.name
      when 'String'
        Sociable.configuration.handlers.include?(kind.underscore.to_sym)
      when 'Symbol'
        Sociable.configuration.handlers.include?(kind)
      when 'Class'
        configured_handlers.include?(kind)
      else
        raise ArgumentError, "#{kind} is not a symbol or class"
      end
    end
  end
end