module Sociable
  class Engine < ::Rails::Engine
    isolate_namespace Sociable

    initializer 'sociable.action_controller',
                :before => :load_config_initializers do
      ::ActiveSupport.on_load :action_controller do
        ::ActionController::Base.helper SociableHelper
      end
    end

    initializer 'sociable.extend_active_record',
                :after => :load_config_initializers do
      ::ActiveSupport.on_load :active_record do
        ::ActiveRecord::Base.extend(::Sociable::ActiveRecord::ClassMethods)
      end
    end
  end
end
