namespace :sociable do
  desc "Reparse all social links"
  task :refresh => :environment do
    Sociable::Link.find_each do |sl|
      sl.kind(reload: true)
      sl.save
      puts "Invalid Address -- id: #{sl.id} address: #{sl.address}" if sl.errors.any?
    end
  end
end