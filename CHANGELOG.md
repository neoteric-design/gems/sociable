# Sociable Changelog

## 0.5.0

* Fix possible loading issue with zeitwerk

## 0.4.2

* Ruby 3 fixes

## 0.4.1

* Fix conflict with rails-api, only include helpers on ActionController::Base

## 0.4.0
* Remove fawesome_icon helper, use font awesome rails' own helper instead
* Rename handler icon classes for above
* Allow sociable_link_to helper to take a block to customize output

## 0.3.3

* Fix copypaste error on active_admin generator

## 0.3.2

* Add active_admin generator for importing assets

## 0.3.1

* Fix: missing class on form

## 0.3.0
* Fix: Ruby 2.4.0 compatible
* Fix: Account for nils when assigning address
* Add Formtastic input
* Add `by` and `primary` scopes
* Add `sociable_primary` convenience method
* Improve: Form display and JS

## 0.2.1 (pre)

* Remove IE8 and earlier compatibility for admin stylesheet

## 0.2.0

* Rails 5 Compaitibility
