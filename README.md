# ⚠️ Archival Notice ⚠️ 

This project was a part of Neoteric Design's internal tooling and prototypes. 

These are no longer officially supported or maintained, and may contain bugs or be in any stage of (in)completeness.

This repository is provided as a courtesy to our former clients to support their projects going forward, as well in the interest of giving back what we have to the community. If you found yourself here, we hope you find it useful in some capacity ❤️ 


--------------------

# Sociable

Social link engine for the Neoteric Design CMS.

Attach one or many social links to a model. Social links can be a Twitter profile, a phone number, an email address, or any web URL. Sociable will autodetect what kind of link it is from its dozen of built-in matchers. And it's easy to add your own.

## Installation

Neoteric CMS gems are served from a private host. Replace `SECURE_GEMHOST` with the source address.

```ruby
# Gemfile
source SECURE_GEMHOST do
  gem 'sociable', '~> 0.2.0'
end
```

```sh
$ bundle install
$ rails g sociable:install
$ rake db:migrate
```

## Usage

Sociable can be attached to any model.

```ruby
# app/models/person.rb
class Person < ActiveRecord::Base
  sociable
end
```

### Setting up your form

Sociable comes with a basic set of assets and helpers to get your form up, especially when using ActiveAdmin.

* Assets: Include `sociable/admin.js` and `sociable/admin.scss`
* Strong parameters: adding `Sociable.params` to your parent models whitelist will take care of the appropriate nested attributes
* Rendering form: `<%= sociable_form(form) %>`

#### ActiveAdmin Example
```erb
<!-- app/views/admin/people/_form.html.erb -->

<%= semantic_form_for [:admin, @person] do |f| %>
  <%= f.input :name %>
  <!-- .. -->
  <%= sociable_form(f) %>
  <%= f.actions %>
<% end %>
```

```coffeescript
# app/assets/javascripts/active_admin.js.coffeescript

# ...
#= require sociable/admin
# ...
```

```scss
// app/assets/stylesheets/active_admin.css.scss

// ...
@import 'sociable/admin';
// ...
```

### Rendering links

Sociable comes with a few helpers to easily and consistenly render links.



```erb
Built in Font Awesome support
<%= sociable_icon @social_link, 'optional-list of-additional-classes' %>

Customized link_to method that automatically handles phone numbers and email mailto's
<%= sociable_link_to @social_link %>

Iteration example
<ul>
  <% @person.social_links.each do |link| %>
    <li><%= sociable_icon(link) %><%= sociable_link_to(link) %></li>
  <% end %>
</ul>
```

## Configuration

You can configure which handlers are active, as well set your own Font Awesome icon for any given handler. See [handlers](#handlers) for customizing your own.

```ruby
Sociable.configure do
  # Set the available handlers, looser matchers last
  config.handlers = [:phone_number, :email_address, :linked_in]

  # Override Font Awesome icon classes
  config.icons = { facebook: 'some-other-icon', tumblr: 'tumblr-alt' }
end
```


## Handlers

Handlers represent a specific kind of social link. Setting up a handler for a given service allows you to customize how that link is, well, handled. Notably, you can assign it a specific icon.

Built in handlers

* Generic web link
* Phone
* Email
* Twitter
* Facebook
* YouTube
* LinkedIn
* Instagram
* Vimeo
* Tumblr
* Skype

### Matching

Sociable iterates over each configured handler **in the order listed** and returns the first match, not unlike Rails routes. As such, it's a good idea to place looser matches towards the end of the array, unless the intent is to cast a wide net :)

### Extending

You are free to add your own matchers! Here's a simple example

```ruby
module Sociable
  module Handlers
    # Inheriting from Generic does most of the work for web links
    class HipsterShareHandler < Generic

      # At a minimum it needs to implement the matches? class method.
      def self.matches?(str)
        str =~ /hipstershare.com/i
      end

      def default_icon_name
        'space-shuttle'
      end

    end
  end
end


# Don't forget to make it available
Sociable.configuration.handlers.prepend(:hipster_share)
```

See [lib/sociable/handlers.rb](lib/sociable/handlers.rb) for how the built in handlers are made.
