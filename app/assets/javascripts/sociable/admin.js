attachSortable = function(parent) {
  parent.sortable({
    items: 'li.sociable-link',
    update: function(e, ui) {
      parent.find('.position').each(function(idx) {
        $(this).val(idx);
      });
    }
  });
};

$(function() {
  attachSortable($('body').find('.sociable'));

  $(document).on('click', '.sociable a.remove', function(e) {
    e.preventDefault();
    slink   = $(this).closest('li.sociable-link');
    _destroy = slink.find('.destroy');

    slink.addClass('highlight');

    if(confirm('Are you sure you want to delete this?')) {
      _destroy.val(1);
      slink.fadeOut();
    } else {
      slink.removeClass('highlight');
    }
  });

  $(document).on('click', '.add-sociable', function(e) {
    e.preventDefault();

    slinks     = $(this).closest('.sociable');
    lastSlink  = slinks.children('li.sociable-link').last();
    size       = slinks.children('li.sociable-link').length;
    newSlink   = lastSlink.clone();

    $(newSlink).find('input').each(function(){
      replacer = /(.*\W+)(\d+)(\W+.*)/i;
      newId = $(this).attr('id').replace(replacer, '$1' + size + '$3');
      newName = $(this).attr('name').replace(replacer, '$1' + size + '$3');
      $(this).val('').attr('name', newName).attr('id', newId);
    });

    $(newSlink).insertAfter(lastSlink).slideDown();
  });

  // ActiveAdmin has_many support
  $(document).on('click', '.has_many_add', function(e) {
    attachSortable($(this).siblings('fieldset:last').find('.sociable'));
  });
});
