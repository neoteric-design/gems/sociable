module SociableHelper
  def sociable_icon(sociable_link, **options)
    fa_icon sociable_link.icon_name, options
  end

  def sociable_link_to(social_link, **html_options, &block)
    if social_link.is_url?
      sociable_link_to_url(social_link, **html_options, &block)
    else
      sociable_link_to_nonurl(social_link, **html_options, &block)
    end
  end

  def sociable_link_to_url(social_link, **html_options, &block)
    if block_given?
      link_to social_link.address, **html_options, &block
    else
      link_to social_link.address, social_link.address, **html_options
    end
  end

  def sociable_link_to_nonurl(social_link, **html_options, &block)
    if block_given?
      content_tag :span, social_link.address, **html_options, &block
    else
      content_tag :span, social_link.address, **html_options
    end
  end

  def sociable_form(form)
    render('sociable/form', f: form)
  end
end
