module Sociable
  class Link < ::ActiveRecord::Base
    include Sortable

    belongs_to :social_linkable, polymorphic: true, optional: true

    validates_presence_of :address
    validate :valid_url?

    delegate :icon_name, :is_url?, to: :handler, prefix: nil

    def self.by(kind)
      where(kind: kind.to_s.classify)
    end

    def self.primary(kind)
      by(kind).order(:position).first
    end

    def handler(**options)
      if kind.to_s.empty? || options[:reload]
        @handler = Handlers.parse(address)
      elsif Handlers.include? kind
        @handler ||= Handlers.find(kind).new
      else
        @handler = Handlers::Generic.new
      end
    end

    def valid_url?
      if address.present? && handler(reload: true).is_url?
        begin
          u = URI.parse(address)
          address.prepend "http://" unless u.scheme
        rescue
          errors.add(:address, "Sorry! That doesn't seem to be a valid URL")
        end
      end
    end

    def address=(str)
      super(str.to_s.strip)
      self[:kind] = handler(reload: true).kind
    end

    def kind(**options)
      return self[:kind] unless options[:reload]
      self[:kind] = handler(reload: true).kind
    end
  end
end
